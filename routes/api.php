<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::post('/login', 'Auth\ApiAuthController@login');
Route::post('/registration', 'Auth\ApiAuthController@registration');
Route::get('/settings', 'SettingsController@settings');

Route::group(['middleware' => ['auth:api']], function () {
    Route::get('/blood-group-chart', 'SettingsController@bloodGroupChart');
    Route::get('/profile', 'ProfileController@profile');
    Route::post('/update-profile', 'ProfileController@updateProfile');
    Route::post('/update-password', 'ProfileController@updatePassword');
    Route::post('/update-profile-photo', 'ProfileController@updateProfilePhoto');
    Route::post('/update-last-donate-date', 'ProfileController@updateLastDonatedDate');
    Route::post('/find-donors', 'DonorsController@findDonors');
    Route::get('/make-ready-donor/{profile_id}', 'DonorsController@makeReadyDonor');
    Route::get('/donor-profile/{profile_id}', 'DonorsController@donorProfile');
    Route::post('/donor-profile-update/{profile_id}', 'DonorsController@updateProfileUpdate');
    Route::post('/donor-profile-update-last-donate-date/{profile_id}', 'DonorsController@donorProfileUpdateLastDonateDate');
});