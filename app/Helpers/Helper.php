<?php
/*
use Illuminate\Support\Facades\DB;
try{
    DB::beginTransaction();
    DB::commit();
}catch(\Exception $e){
    DB::rollback();
}
*/

/**
 * base64 image upload function
 */
function base64_to_image($base64_string, $location)
{
    $filename = time().rand(100,999).".jpg";
    $local_path  = $_SERVER['DOCUMENT_ROOT'];

    $path        = env('APP_PATH')."/public/" . $location . "/" . $filename;
    $output_file = $local_path . "/" . $path; //save to local address

    // open the output file for writing
    $ifp = fopen($output_file, 'wb');
    
    $data = explode(',', $base64_string);
    if(sizeof($data) > 1)
    {
        // we could add validation here with ensuring count( $data ) > 1
        fwrite($ifp, base64_decode($data[1]));
        // clean up the file resource
        fclose($ifp);
    }
    else
    {
        $filename = NULL;
    }

    return $filename;
}

function TrxId()
{
    return chr(rand(65,90)).substr(time(), 1, -1).chr(rand(65,90));
}