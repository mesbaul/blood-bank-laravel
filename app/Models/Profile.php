<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Profile extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'blood_group',
        'gender',
        'dob',
        'last_donate_at',
        'address',
        'photo',
    ];

    public function relUser()
    {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }

    // photo_url
    public function getPhotoUrlAttribute()
    {
        return $this->photo ? asset('/public/upload/'. $this->photo) : asset('/public/upload/black-thumbnail.png');
    }

    // age
    public function getAgeAttribute()
    {
        return Carbon::parse($this->dob)->diff(Carbon::now('Asia/Dhaka'))->format('%y years old');
    }

    // days_of_donate
    public function getDaysOfDonateAttribute()
    {
        $days_of_donate = (!empty($this->last_donate_at)) ? Carbon::now('Asia/Dhaka')->diffInDays($this->last_donate_at) : NULL;
        if(!empty($this->last_donate_at))
        {
            return ($days_of_donate == 0) ? 'Today' : $days_of_donate . ' days ago';
        }
        return NULL;
    }
}
