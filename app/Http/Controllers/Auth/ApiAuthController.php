<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use App\Models\Profile;
use App\Http\Resources\UserResource;
use Carbon\Carbon;

class ApiAuthController extends Controller
{
    public function login(Request $request)
    {
        $data = $request->validate([
            'phone' => 'required|exists:users,phone|size:11|regex:/(01)[0-9]{9}/',
            'password' => 'required',
        ]);
        
        if (auth()->attempt(['phone' => $data['phone'], 'password' => $data['password']])) {
            $token = auth()->user()->createToken('authToken')->accessToken;
            return response()->json(['user' => new UserResource(auth()->user()), 'access_token' => $token, 'message' => 'Your authentation has been successfull.'], 200);
        }
        return response()->json(['message' => 'Please write down right credentials'], 400);
    }

    public function registration(Request $request)
    {
        $data = $request->validate([
            'name' => 'required|max:100',
            'phone' => 'required|unique:users,phone|size:11|regex:/(01)[0-9]{9}/',
            'address' => 'required|max:100',
            'blood_group' => 'required|max:10',
            'role' => 'required|max:100',
            'gender' => 'required|max:10',
            'dob' => 'required|date_format:Y-m-d',
        ]);
        $age = Carbon::parse($request->dob)->diff(Carbon::now('Asia/Dhaka'))->format('%y');
        if($age < 18)
        {
            return response()->json(['message' => 'Your age is not yet eligible for blood donation. Must be at least 18 years old'], 400);
        }
        try{
            DB::beginTransaction();

            $user = User::create([
                'name' => $request->name,
                'phone' => $request->phone,
                'password' => bcrypt('12345678'),
                'role' => $request->role,
            ]);

            $photo = NULL;
            if ($request->has('photo')) {
                $photo = base64_to_image($request->photo, 'upload');
            }

            Profile::create([
                'user_id' => $user->id,
                'blood_group' => $request->blood_group,
                'gender' => $request->gender,
                'dob' => $request->dob,
                'last_donate_at' => Carbon::today('Asia/Dhaka')->subDays(30)->toDateString(),
                'address' => $request->address,
                'photo' => $photo,
            ]);

        DB::commit();
            return response()->json(['message' => 'Registration has been completed.'], 200);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json(['message' => $e.'Registration has been failed. Please try again'], 400);
        }
    }
}