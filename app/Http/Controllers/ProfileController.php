<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use App\Models\Profile;
use App\Models\DonateHistory;
use App\Http\Resources\UserResource;
use Carbon\Carbon;

class ProfileController extends Controller
{
    public function profile()
    {
        return response()->json(new UserResource(auth()->user()));
    }

    public function updateProfile(Request $request)
    {
        $id = auth()->user()->id;
        $data = $request->validate([
            'phone' => 'required|size:11|regex:/(01)[0-9]{9}/|unique:users,phone,'.$id,
            'address' => 'required|max:100',
        ]);
        
        try{
            DB::beginTransaction();

            $user = User::where('id', $id)->update([
                'phone' => $request->phone,
            ]);
            
            Profile::where('user_id', $id)->update([
                'address' => $request->address,
            ]);


        DB::commit();
            return response()->json(['message' => 'Profile has been update successfull.'], 200);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json(['message' => 'Profile has been update failed. Please try again'], 400);
        }
    }

    public function updatePassword(Request $request)
    {
         $data = $request->validate([
            'password' => 'required|confirmed|min:8',
        ]);

        $password = $request->password;
        $id = auth()->user()->id;

        try{
            DB::beginTransaction();
            User::where(['id' => $id])->update([
                'password' => bcrypt($password)
            ]);
        DB::commit();
            return response()->json(['message' => 'Password has been change successfull'], 200);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json(['message' => 'Password has been change failed. Please try again'], 400);
        }
    }

    public function updateProfilePhoto(Request $request)
    {
        $data = $request->validate([
            'photo' => 'required',
        ]);
        
        try{
            DB::beginTransaction();

            $photo = NULL;
            if ($request->has('photo')) {
                $photo = base64_to_image($request->photo, 'upload');
            }

            $profile = Profile::where('user_id', auth()->user()->id)->first();
            if(!empty($profile->photo))
            {
                $photo_path = public_path('upload/'.$profile->photo);
                if (file_exists($photo_path))
                {
                    unlink($photo_path);
                }
            }
            
            Profile::where('user_id', auth()->user()->id)->update([
                'photo' => $photo,
            ]);
            
        DB::commit();
            return response()->json(['message' => 'Profile photo update has been successfull'], 200);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json(['message' => $e.'Profile photo update has been failed. Please try again'], 400);
        }
    }

    public function updateLastDonatedDate(Request $request)
    {
        $data = $request->validate([
            'last_donate_date' => 'required|date_format:Y-m-d',
        ]);

        $auth_user = auth()->user();
        $last_donate_date = $request->last_donate_date;
        $days_of_donate = $auth_user->relProfile->days_of_donate;

        if(!empty($days_of_donate) && $days_of_donate < 30)
        {
            return response()->json(['message' => 'You just donated at '.$days_of_donate.'. Please try later'], 400);
        }
        
        try{
            DB::beginTransaction();
            DonateHistory::where(['user_id' => auth()->user()->id, 'donate_date' => $last_donate_date])->delete();
            DonateHistory::create([
                'user_id' => auth()->user()->id,
                'donate_date' => $last_donate_date,
            ]);
            
            Profile::where('user_id', auth()->user()->id)->update([
                'last_donate_at' => $last_donate_date,
            ]);
            
        DB::commit();
            return response()->json(['message' => 'Donation date update.', 'date' => Carbon::parse($last_donate_date)->format('d F Y')], 200);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json(['message' => 'Registration has been failed. Please try again'], 400);
        }
    }
}
