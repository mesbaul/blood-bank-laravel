<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Models\Profile;
use App\Models\User;
use App\Models\DonateHistory;
use App\Http\Resources\DonorsResource;
use Carbon\Carbon;

class DonorsController extends Controller
{
    public function findDonors(Request $request)
    {
        $donors = Profile::whereDate('last_donate_at', '<=', Carbon::today('Asia/Dhaka')->subDays(30)->toDateString());
        if($request->ready_donor == true)
        {
            $donors->where('ready_for_donate', 'yes');
        }
        else
        {
            $donors->where('ready_for_donate', 'no');
        }
        if(!empty($request->gender))
        {
            $donors->whereIn('gender', $request->gender);
        }
        if(!empty($request->blood_group))
        {
            $donors->whereIn('blood_group', $request->blood_group);
        }
        $x = $donors->orderBy('id', 'desc')->get();
        return response()->json(DonorsResource::collection($x));
    }

    public function makeReadyDonor($id)
    {
        $donor = Profile::where('id', $id)->first();
        
        if(empty($donor))
        {
            return response()->json(['message' => 'Donor profile has not found. Please try again'], 400);
        }

        $is_ready = $donor->ready_for_donate;
        try{
            DB::beginTransaction();
            Profile::where('id', $id)->update([
                'ready_for_donate' => ($is_ready == 'yes') ? 'no' : 'yes',
            ]);
            $message = ($is_ready == 'yes') ? "leave" : "enlisted";
        DB::commit();
            return response()->json(['message' => 'Donor has been '.$message.' successful'], 200);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json(['message' => 'Donor has been '.$message.' failed. Please try again'], 400);
        }
    }

    public function donorProfile($id)
    {
        return response()->json(new DonorsResource(Profile::find($id)));
    }

    public function updateProfileUpdate(Request $request, $profile_id)
    {
        $profile = Profile::where('id', $profile_id)->first();

        if(empty($profile))
        {
            return response()->json(['message' => 'Donor profile has not found. Please try again'], 400);
        }

        $id = $profile->relUser->id;

        $data = $request->validate([
            'name' => 'required|max:100',
            'phone' => 'required|size:11|regex:/(01)[0-9]{9}/|unique:users,phone,'.$id,
            'address' => 'required|max:100',
            'blood_group' => 'required|max:10',
            'role' => 'required|max:100',
            'gender' => 'required|max:10',
            'dob' => 'required|date_format:Y-m-d',
        ]);
        
        try{
            DB::beginTransaction();
            $user = User::where('id', $id)->update([
                'name' => $request->name,
                'phone' => $request->phone,
                'role' => $request->role,
            ]);

            $photo = NULL;
            if ($request->has('photo')) {
                $photo = base64_to_image($request->photo, 'upload');
            }

            if(!empty($profile->photo))
            {
                $photo_path = public_path('upload/'.$profile->photo);
                if (file_exists($photo_path))
                {
                    unlink($photo_path);
                }
            }
            
            Profile::where('user_id', $profile_id)->update([
                'blood_group' => $request->blood_group,
                'gender' => $request->gender,
                'dob' => $request->dob,
                'address' => $request->address,
                'photo' => $photo,
            ]);

        DB::commit();
            return response()->json(['message' => 'Profile update has been completed.'], 200);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json(['message' => 'Profile update has been failed. Please try again'], 400);
        }
    }

    public function donorProfileUpdateLastDonateDate(Request $request, $profile_id)
    {
        $data = $request->validate([
            'last_donate_date' => 'required|date_format:Y-m-d',
        ]);

        $last_donate_date = $request->last_donate_date;
        $profile = Profile::where('id', $profile_id)->first();
        if(empty($profile))
        {
            return response()->json(['message' => 'Donor profile has not found. Please try again'], 400);
        }
        
        $days_of_donate = $profile->days_of_donate;
        
        if(!empty($days_of_donate) && $days_of_donate < 30)
        {
            return response()->json(['message' => 'You just donated at '.$days_of_donate.'. Please try later'], 400);
        }

        try{
            DB::beginTransaction();
            $user_id = $profile->relUser->id;
            DonateHistory::where(['user_id' => $user_id, 'donate_date' => $last_donate_date])->delete();
            DonateHistory::create([
                'user_id' => $user_id,
                'donate_date' => $last_donate_date,
            ]);
            
            Profile::where('id', $profile_id)->update([
                'last_donate_at' => $last_donate_date,
            ]);
            
        DB::commit();
            return response()->json(['message' => 'Donation date update.', 'date' => Carbon::parse($last_donate_date)->format('d F Y')], 200);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json(['message' => 'Registration has been failed. Please try again'], 400);
        }
    }
    
}
