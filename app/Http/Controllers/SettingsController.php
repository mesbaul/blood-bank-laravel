<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Models\Profile;
use Carbon\Carbon;

class SettingsController extends Controller
{
    public $blood_groups = ['A+', 'A-', 'B+', 'B-', 'O+', 'O-', 'AB+', 'AB-'];

    public function settings()
    {
        return response()->json([
            'blood_groups' => $this->blood_groups,
            'roles' => ['founder', 'admin', 'modarator', 'donor'],
            'genders' => ['Male', 'Female', 'Common'],
            'organization' => [
                'name' => 'XYZ Blood Bank',
                'logo' => asset('/public/logo.png'),
                'email' => 'xyzbloodbank@gmail.com',
                'web' => 'https://www.xyzbloodbank.com',
                'age' => Carbon::parse('2020-01-01')->diff(Carbon::now('Asia/Dhaka'))->format('%y years, %m months %d days'),
                'facebook' => 'https://www.facebook.com/groups/xyzbloodbank',
                'slogan' => 'Our blood bank group is an non profitable organization that works for collection blood donors and distribute.',
            ]
        ]);
    }

    public function bloodGroupChart()
    {
        $exists_group_array = Profile::select('blood_group', DB::raw('count(*) as total'))->groupBy('blood_group')->pluck('total', 'blood_group')->toArray();
        $exists_male_gender_array = Profile::select('blood_group', DB::raw('count(*) as total'))->where('gender', 'Male')->groupBy('blood_group')->pluck('total', 'blood_group')->toArray();
        $exists_female_gender_array = Profile::select('blood_group', DB::raw('count(*) as total'))->where('gender', 'Female')->groupBy('blood_group')->pluck('total', 'blood_group')->toArray();
        $exists_ready_donor_array = Profile::select('blood_group', DB::raw('count(*) as total'))->where('ready_for_donate', 'yes')->groupBy('blood_group')->pluck('total', 'blood_group')->toArray();
        foreach($this->blood_groups as $group)
        {
            $result['labels'][] = $group;
            $total_group[] = (array_key_exists($group, $exists_group_array)) ? $exists_group_array[$group] : 0;
            $total_male_gender[] = (array_key_exists($group, $exists_male_gender_array)) ? $exists_male_gender_array[$group] : 0;
            $total_female_gender[] = (array_key_exists($group, $exists_female_gender_array)) ? $exists_female_gender_array[$group] : 0;
            $total_ready_donor[] = (array_key_exists($group, $exists_ready_donor_array)) ? $exists_ready_donor_array[$group] : 0;
        }
        $result['datasets'] = [
            [
                'label' => 'Donors',
                'backgroundColor' => '#f44336',
                'data' => $total_group,
            ],
            [
                'label' => 'Male Gender',
                'backgroundColor' => '#8D47C4',
                'data' => $total_male_gender,
            ],
            [
                'label' => 'Female Gender',
                'backgroundColor' => '#641D47',
                'data' => $total_female_gender,
            ],
            [
                'label' => 'Ready Donors',
                'backgroundColor' => '#01AF56',
                'data' => $total_ready_donor,
            ],
        ];
        return response()->json($result);
    }
}
