<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Carbon\Carbon;

class DonorsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->relUser->name,
            'phone' => $this->relUser->phone,
            'blood_group' => $this->blood_group,
            'dob' => $this->dob,
            'days_of_donate' => ($this->relUser->total_donate > 0) ? $this->days_of_donate : 'You haven\'t any donations yet',
            'last_donate_at' => ($this->relUser->total_donate > 0) ? Carbon::parse($this->last_donate_at)->format('d F Y') : 'You haven\'t any donations yet',
            'age' => $this->age,
            'gender' => $this->gender,
            'photo' => $this->photo_url,
            'address' => (!empty($this->address)) ? $this->address : 'Address not available',
            'ready_for_donate' => $this->ready_for_donate,
            'role' => $this->relUser->role,
            'total_donate' => $this->relUser->total_donate,
        ];
    }
}
