<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Carbon\Carbon;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'phone' => $this->phone,
            'blood_group' => $this->relProfile->blood_group,
            'dob' => $this->relProfile->dob,
            'last_donate_at' => ($this->total_donate > 0) ? Carbon::parse($this->relProfile->last_donate_at)->format('d F Y') : 'You haven\'t any donations yet',
            'age' => $this->relProfile->age,
            'gender' => $this->relProfile->gender,
            'photo' => $this->relProfile->photo_url,
            'address' => $this->relProfile->address,
            'ready_for_donate' => $this->relProfile->ready_for_donate,
            'role' => $this->role,
            'total_donate' => $this->total_donate,
        ];
    }
}
